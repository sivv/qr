object frmSimpleMain: TfrmSimpleMain
  Left = 0
  Top = 0
  Caption = 'Simple QR Example'
  ClientHeight = 414
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = txtDataChange
  DesignSize = (
    385
    414)
  PixelsPerInch = 96
  TextHeight = 13
  object imgQR: TImage
    Left = 8
    Top = 35
    Width = 369
    Height = 369
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object txtData: TEdit
    Left = 8
    Top = 8
    Width = 369
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'https://bitbucket.org/sivv/qr'
    OnChange = txtDataChange
  end
end
