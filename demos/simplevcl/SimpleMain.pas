unit SimpleMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TfrmSimpleMain = class(TForm)
    txtData: TEdit;
    imgQR: TImage;
    procedure txtDataChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSimpleMain: TfrmSimpleMain;

implementation

uses qr.code.vcl;

{$R *.dfm}

procedure TfrmSimpleMain.FormCreate(Sender: TObject);
begin
  txtDataChange(nil);
end;

procedure TfrmSimpleMain.txtDataChange(Sender: TObject);
var
  bmp : TQRBitmap;
begin
  bmp := TQRBitmap.Create(txtData.Text, imgQR.Width, imgQR.Height);
  try
    bmp.ShowBorder := True;
    imgQR.Picture.Graphic := bmp;
  finally
    bmp.Free;
  end;
end;

end.
