//Copyright (c) 2019 by Jason Southwell
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

unit qr.code.vcl;

interface

uses
  System.SysUtils, System.Classes, VCL.Graphics, qr.code, VCL.Imaging.PNGImage;

type
  TQRBitmap = class(TBitmap)
  private
    FQR : TQRCode;
    FShowBorder: boolean;
    FShowModuleGrid: boolean;
    function GetData: TArray<Byte>;
    function GetECL: TErrorCorrectionLevel;
    function GetMask: TMask;
    function GetText: string;
    function GetVersion: TVersion;
    procedure SetData(const Value: TArray<Byte>);
    procedure SetECL(const Value: TErrorCorrectionLevel);
    procedure SetMask(const Value: TMask);
    procedure SetText(const Value: string);
    procedure SetVersion(const Value: TVersion);
    procedure SetShowBorder(const Value: boolean);
    procedure SetShowModuleGrid(const Value: boolean);
  public
    constructor Create(AText : string; AWidth, AHeight : integer); overload;
    constructor Create(AText : string); overload;
    constructor Create(AText : string; APixelsPerModule : integer); overload;
    constructor Create(AData : TArray<Byte>; AWidth, AHeight : integer); overload;
    constructor Create(AData : TArray<Byte>); overload;
    constructor Create(AData : TArray<Byte>; APixelsPerModule : integer); overload;
    constructor Create; overload; override;
    destructor Destroy; override;

    procedure BeginUpdate;
    procedure EndUpdate;

    property Text : string read GetText write SetText;
    property Data : TArray<Byte> read GetData write SetData;

    property Version : TVersion read GetVersion write SetVersion;
    property ECL : TErrorCorrectionLevel read GetECL write SetECL;
    property QRMask : TMask read GetMask write SetMask;

    property ShowModuleGrid : boolean read FShowModuleGrid write SetShowModuleGrid;
    property ShowBorder : boolean read FShowBorder write SetShowBorder;
  end;

  TQRPngImage = class(TPngImage)
  public
    constructor Create(AText : string; AWidth, AHeight : integer); overload;
    constructor Create(AText : string); overload;
    constructor Create(AText : string; APixelsPerModule : integer); overload;
    constructor Create(AData : TArray<Byte>; AWidth, AHeight : integer); overload;
    constructor Create(AData : TArray<Byte>); overload;
    constructor Create(AData : TArray<Byte>; APixelsPerModule : integer); overload;
  end;

implementation

uses System.Math, System.Types;

{ TQRCodeBitmap }

procedure TQRBitmap.BeginUpdate;
begin
  FQR.BeginUpdate;
end;

constructor TQRBitmap.Create;
begin
  inherited Create;
  FQR := TQRCode.Create;
  FQR.OnPaint :=
    procedure(Width, Height : Integer; BlackRects : TArray<TRect>)
    var
      R : TRect;
      x, y : integer;
    begin
      Self.Width := Width;
      Self.Height := Height;

      Self.Canvas.Lock;
      try
        Self.Canvas.Brush.Color := clWhite;
        Self.Canvas.Pen.Color := clWhite;
        Self.Canvas.FillRect(TRect.Create(0,0,Self.Width, Self.Height));

        Self.Canvas.Brush.Color := clBlack;
        Self.Canvas.Pen.Color := clBlack;

        for R in BlackRects do
        begin
          Self.Canvas.FillRect(R);
        end;

        if FShowModuleGrid then
        begin
          Self.Canvas.Pen.Color := clLtGray;
          Self.Canvas.Pen.Style := TPenStyle.psDot;
          for x := 0 to Self.Width -1 do
            if x mod FQR.PixelsPerModule = 0 then
            begin
              Self.Canvas.MoveTo(x,0);
              Self.Canvas.LineTo(x,Self.Height-1);
            end;
          for y := 0 to Self.Height-1 do
          begin
            if y mod FQR.PixelsPerModule = 0 then
            begin
              Self.Canvas.MoveTo(0,y);
              Self.Canvas.LineTo(Self.Height-1, y);
            end;
          end;
        end;

        if FShowBorder then
        begin
          Self.Canvas.Brush.Color := clBlack;
          Self.Canvas.Pen.Color := clBlack;
          Self.Canvas.FrameRect(TRect.Create(0,0,Self.Width, Self.Height));
        end;
      finally
        Self.Canvas.Unlock;
      end;

    end;
end;

constructor TQRBitmap.Create(AText: string; APixelsPerModule: integer);
begin
  Create;
  FQR.BeginUpdate;
  try
    FQR.Text := AText;
    FQR.PixelsPerModule := APixelsPerModule;
  finally
    FQR.EndUpdate;
  end;
end;

constructor TQRBitmap.Create(AData: TArray<Byte>; AWidth, AHeight: integer);
begin
  Create;
  FQR.BeginUpdate;
  try
    FQR.Data := AData;
    FQR.RenderSize := Min(AWidth, AHeight);
  finally
    FQR.EndUpdate;
  end;
end;

constructor TQRBitmap.Create(AData: TArray<Byte>; APixelsPerModule: integer);
begin
  Create;
  FQR.BeginUpdate;
  try
    FQR.Data := AData;
    FQR.PixelsPerModule := APixelsPerModule;
  finally
    FQR.EndUpdate;
  end;
end;

constructor TQRBitmap.Create(AData: TArray<Byte>);
begin
  Create;
  FQR.Data := AData;
end;

constructor TQRBitmap.Create(AText: string);
begin
  Create;
  FQR.Text := AText;
end;

constructor TQRBitmap.Create(AText: string; AWidth, AHeight: integer);
begin
  Create;
  FQR.BeginUpdate;
  try
    FQR.Text := AText;
    FQR.RenderSize := Min(AWidth, AHeight);
  finally
    FQR.EndUpdate;
  end;
end;

destructor TQRBitmap.Destroy;
begin
  FQR.Free;
  inherited;
end;

procedure TQRBitmap.EndUpdate;
begin
  FQR.EndUpdate;
end;

function TQRBitmap.GetData: TArray<Byte>;
begin
  Result := FQR.Data;
end;

function TQRBitmap.GetECL: TErrorCorrectionLevel;
begin
  Result := FQR.ECL;
end;

function TQRBitmap.GetMask: TMask;
begin
  Result := FQR.Mask;
end;

function TQRBitmap.GetText: string;
begin
  Result := FQR.Text;
end;

function TQRBitmap.GetVersion: TVersion;
begin
  Result := FQR.Version;
end;

procedure TQRBitmap.SetData(const Value: TArray<Byte>);
begin
  FQR.Data := Value;
end;

procedure TQRBitmap.SetECL(const Value: TErrorCorrectionLevel);
begin
  FQR.ECL := Value;
end;

procedure TQRBitmap.SetMask(const Value: TMask);
begin
  FQR.Mask := Value;
end;

procedure TQRBitmap.SetShowBorder(const Value: boolean);
begin
  FShowBorder := Value;
  FQR.Redraw;
end;

procedure TQRBitmap.SetShowModuleGrid(const Value: boolean);
begin
  FShowModuleGrid := Value;
  FQR.Redraw;
end;

procedure TQRBitmap.SetText(const Value: string);
begin
  FQR.Text := Value;
end;

procedure TQRBitmap.SetVersion(const Value: TVersion);
begin
  FQR.Version := Value;
end;

{ TQRPngImage }

constructor TQRPngImage.Create(AText: string; APixelsPerModule: integer);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AText, APixelsPerModule);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

constructor TQRPngImage.Create(AText: string);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AText);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

constructor TQRPngImage.Create(AText: string; AWidth, AHeight: integer);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AText, AWidth, AHeight);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

constructor TQRPngImage.Create(AData: TArray<Byte>; APixelsPerModule: integer);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AData, APixelsPerModule);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

constructor TQRPngImage.Create(AData: TArray<Byte>);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AData);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

constructor TQRPngImage.Create(AData: TArray<Byte>; AWidth, AHeight: integer);
var
  bmp : TQRBitmap;
begin
  inherited Create;
  bmp := TQRBitmap.Create(AData, AWidth, AHeight);
  try
    Assign(bmp);
  finally
    bmp.Free;
  end;
end;

end.
